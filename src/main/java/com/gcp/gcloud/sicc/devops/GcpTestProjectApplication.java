package com.gcp.gcloud.sicc.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class GcpTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(GcpTestProjectApplication.class, args);
	}

}
