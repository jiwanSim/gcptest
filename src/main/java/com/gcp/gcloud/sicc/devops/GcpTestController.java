package com.gcp.gcloud.sicc.devops;

import java.security.Principal;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableWebSecurity
//@SpringBootApplication
public class GcpTestController {

	@RequestMapping("/")
	public String GcpTest(Principal principal) {
//		return "GcpTest";
		return principal.toString();
	} 
}
